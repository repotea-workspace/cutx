import 'dart:io';
import 'dart:typed_data';

import 'package:cutx/cutx.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter_test/flutter_test.dart';
import 'package:path_provider/path_provider.dart';

Future<Uint8List?> networkImage(String url) async {
  dio.Response<List<int>> response = await dio.Dio().get(
    url,
    options: dio.Options(responseType: dio.ResponseType.bytes),
  );
  List<int>? data = response.data;
  if (data == null) return null;
  return Uint8List.fromList(data);
}

Future<void> writeImage(Uint8List bytes) async {
  var imageName = DateTime.now().microsecondsSinceEpoch.toString();
  var appDocDir = await getApplicationDocumentsDirectory();
  String basePath = appDocDir.path + '/cutx';
  Directory baseFolder = Directory(basePath);
  if (!baseFolder.existsSync()) {
    baseFolder.createSync(recursive: true);
  }
  String imagePath = basePath + '/' + imageName + '.jpg';
  File imageFile = File(imagePath);
  imageFile.writeAsBytesSync(bytes);
}

void main() {
  // test('adds one to input values', () {
  //   final calculator = Calculator();
  //   expect(calculator.addOne(2), 3);
  //   expect(calculator.addOne(-7), -6);
  //   expect(calculator.addOne(0), 1);
  // });

  test('test cutx', () async {
    var bytes = await networkImage('https://mio.kahub.in/open/cuslate-c.jpg');
    final cutx = Cutx(bytes!,
        direction: const CutDirection.create([
          Direction.top,
          Direction.left,
          Direction.bottom,
          Direction.right,
        ]));
    final bitmap = cutx.cut();
    await writeImage(bitmap!);
  });

  test('test bitmap', () async {
    var bytes = await networkImage('https://mio.kahub.in/open/cuslate-a.png');
    final cutx = Cutx(bytes!);
    final bitmap = cutx.bitmap();
    await writeImage(bitmap!);
  });
}
