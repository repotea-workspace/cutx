import 'dart:math' as math;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;

class Cutx {
  /// Image bytes
  Uint8List bytes;

  /// Margin
  EdgeInsets margin;

  /// Safe color, It will stop when you encounter this color when shrinking the circle.
  int safeColor;

  /// Minimal height
  int? minHeight;

  /// Minimal width
  int? minWidth;

  /// Cut direction
  CutDirection direction;

  Cutx(
    this.bytes, {
    this.margin = const EdgeInsets.all(4),
    this.safeColor = 230,
    this.minWidth,
    this.minHeight,
    this.direction = const CutDirection.all(),
  });

  Uint8List? cut() {
    Uint8List? bitmapBytes = this.bitmap();
    if (bitmapBytes == null) return null;
    img.Image? bitmap = img.decodeImage(bitmapBytes);
    if (bitmap == null) return null;
    var imageWidth = bitmap.width;
    var imageHeight = bitmap.height;

    int x = _detectPosition(bitmap, Direction.left) ?? 0;
    int y = _detectPosition(bitmap, Direction.top) ?? 0;
    int w = _detectPosition(bitmap, Direction.right) ?? imageWidth;
    int h = _detectPosition(bitmap, Direction.bottom) ?? imageHeight;

    if (x == 0 && y == 0 && w == imageWidth && h == imageHeight) {
      return bytes;
    }

    img.Image originImage = img.decodeImage(bytes)!;
    int maybeFinX = x - margin.left.floor() < 0 ? 0 : x - margin.left.floor();
    int maybeFinY = y - margin.top.floor() < 0 ? 0 : y - margin.top.floor();

    maybeFinX = direction.allow(Direction.left) ? maybeFinX : 0;
    maybeFinY = direction.allow(Direction.top) ? maybeFinY : 0;
    int finX = maybeFinX < 0 ? 0 : maybeFinX;
    int finY = maybeFinY < 0 ? 0 : maybeFinY;

    int maybeFinW = (
            // if cut width greater than image with
            w + margin.right.floor() > imageWidth
                ? imageWidth // use image width
                : w + margin.right.floor() // use cut width
        ) -
        finX;
    int maybeFinH = (
            // if cut height greater than image height
            h + margin.bottom.floor() > imageHeight
                ? imageHeight // use image height
                : h + margin.bottom.floor() // use cut height
        ) -
        finY;
    maybeFinW = minWidth ?? maybeFinW;
    maybeFinH = minHeight ?? maybeFinH;
    maybeFinW = maybeFinW > imageWidth ? imageWidth : maybeFinW;
    maybeFinH = maybeFinH > imageHeight ? imageHeight : maybeFinH;

    int finW = direction.allow(Direction.right) ? maybeFinW : imageWidth;
    int finH = direction.allow(Direction.bottom) ? maybeFinH : imageHeight;

    // print('{ x: $x:$finX, y: $y:$finY, w: $w:$finW, h: $h:$finH }');
    img.Image? cropped = img.copyCrop(originImage, finX, finY, finW, finH);
    return Uint8List.fromList(img.encodeJpg(cropped));
  }

  /// Convert to bitmap (Grayscale image)
  Uint8List? bitmap() {
    img.Image? image = img.decodeImage(bytes);
    if (image == null) return null;
    var width = image.width;
    var height = image.height;

    var y = 0;
    while (y < height) {
      for (var x = 0; x < width; x++) {
        int pixel = image.getPixel(x, y);
        int red = (pixel >> 16) & 0xFF;
        int green = (pixel >> 8) & 0xFF;
        int blue = pixel & 0xFF;
        final gray = math.max(math.max(red, green), blue); // light
        // final gray = (red + green + blue) ~/ 3; // middle
        image.setPixelRgba(x, y, gray, gray, gray);
      }
      y += 1;
    }
    return Uint8List.fromList(img.encodeJpg(image));
  }

  int? _detectPosition(img.Image image, Direction direction) {
    var width = image.width;
    var height = image.height;

    bool widthGreaterHeight = width > height;

    if (direction == Direction.left) {
      int? minX;
      if (widthGreaterHeight) {
        /*
        --***************
        vv***************
        -x***************
        v****************
         */
        for (var x = 0; x < width; x++) {
          for (var y = 0; y < height; y++) {
            if (!_checkPrune(image, x, y)) continue;
            minX = minX == null ? x : math.min(x, minX);
          }
        }
      } else {
        /*
        --->--->---
        --->--x****
        ***********
        ***********
        ***********
        ***********
        ***********
         */
        for (var y = 0; y < height; y++) {
          for (var x = 0; x < width; x++) {
            if (!_checkPrune(image, x, y)) continue;
            minX = minX == null ? x : math.min(x, minX);
          }
        }
      }
      return minX;
    }

    if (direction == Direction.right) {
      int? maxX;
      if (widthGreaterHeight) {
        /*
        ***************--
        ***************vv
        ***************x-
        ****************v
         */
        for (var x = width; x-- > 0;) {
          for (var y = 0; y < height; y++) {
            if (!_checkPrune(image, x, y)) continue;
            maxX = maxX == null ? x : math.max(x, maxX);
          }
        }
      } else {
        /*
        ***********
        ***********
        ***********
        ***********
        ***********
        ****x--<---
        ---<---<---
         */
        for (var y = 0; y < height; y++) {
          for (var x = width; x-- > 0;) {
            if (!_checkPrune(image, x, y)) continue;
            maxX = maxX == null ? x : math.max(x, maxX);
          }
        }
      }
      return maxX;
    }

    if (direction == Direction.top) {
      int? minY;
      if (widthGreaterHeight) {
        /*
        --***************
        vv***************
        -y***************
        v****************
         */
        for (var x = 0; x < width; x++) {
          for (var y = 0; y < height; y++) {
            if (!_checkPrune(image, x, y)) continue;
            minY = minY == null ? y : math.min(y, minY);
          }
        }
      } else {
        /*
        --->--->---
        --->--y****
        ***********
        ***********
        ***********
        ***********
        ***********
         */
        for (var y = 0; y < height; y++) {
          for (var x = 0; x < width; x++) {
            if (!_checkPrune(image, x, y)) continue;
            minY = minY == null ? y : math.min(y, minY);
          }
        }
      }
      return minY;
    }

    if (direction == Direction.bottom) {
      int? maxY;
      if (widthGreaterHeight) {
        /*
        --***************
        vv***************
        -y***************
        v****************
         */
        for (var x = 0; x < width; x++) {
          for (var y = height; y-- > 0;) {
            if (!_checkPrune(image, x, y)) continue;
            maxY = maxY == null ? y : math.max(y, maxY);
          }
        }
      } else {
        /*
        ***********
        ***********
        ***********
        ***********
        ***********
        --->--y****
        --->--->---
         */
        for (var y = height; y-- > 0;) {
          for (var x = 0; x < width; x++) {
            if (!_checkPrune(image, x, y)) continue;
            maxY = maxY == null ? y : math.max(y, maxY);
          }
        }
      }
      return maxY;
    }

    return null;
  }

  bool _checkPrune(img.Image image, int x, int y) {
    int pixel = image.getPixel(x, y);
    int red = (pixel >> 16) & 0xFF;
    int green = (pixel >> 8) & 0xFF;
    int blue = pixel & 0xFF;
    return (red < safeColor) || (green < safeColor) || (blue < safeColor);
  }
}

enum Direction {
  left,
  right,
  top,
  bottom,
}

class CutDirection {
  final List<Direction> directions;

  CutDirection(this.directions);

  const CutDirection.all() : directions = Direction.values;

  const CutDirection.create(this.directions);

  bool allow(Direction direction) {
    return directions.contains(direction);
  }
}
